# Copyright (C) 2024 Freetech Solutions

# This file is part of OMniLeads

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
#
---

- name: OMniLeads RESTORE Playbook
  hosts: 
    - omnileads_data
    - omnileads_voice
    - omnileads_app
    - omnileads_aio
  tags:
    - restore
  tasks:

    - name: RESTORE postgres Stop postgresql systemd service 
      service:
        name: postgresql
        state: stopped
      when:
        - inventory_hostname in groups['omnileads_data'] or inventory_hostname in groups['omnileads_aio']
        - container_orchest == "systemd"

    - name: RESTORE postgres drop database podman
      shell: "podman volume rm oml_postgres"
      when:
        - inventory_hostname in groups['omnileads_data'] or inventory_hostname in groups['omnileads_aio']
        - container_orchest == "systemd"
      ignore_errors: yes  

    - name: RESTORE postgres Start postgresql systemd service
      service:
        name: postgresql
        state: started
      when:
        - inventory_hostname in groups['omnileads_data'] or inventory_hostname in groups['omnileads_aio']
        - container_orchest == "systemd"

    # - name: RESTORE wait for postgresql
    #   shell: |
    #     until psql -h {{  }} -U {{ postgres_user }} -c "SELECT 1;" >/dev/null 2>&1; do
    #       echo "Wait for PostgreSQL ..."
    #       sleep 5
    #     done
    #   register: result
    #   retries: 20
    #   delay: 10

    - name: RESTORE postgres DB pg_restore
      shell: "podman run --name pgsql_bk_{{ restore_file_timestamp }} --network=host --env-file /etc/default/django.env -e BACKUP_FILENAME=pgsql-backup-{{ restore_file_timestamp }}.sql --rm {{ omnileads_img }} bash -x /opt/omnileads/bin/init_pgsql_restore.sh"
      when:
        - inventory_hostname in groups['omnileads_app'] or inventory_hostname in groups['omnileads_aio']
        - container_orchest == "systemd"
        
    - name: RESTORE django media_root from selfhosted BUCKET cluster
      shell: "podman exec -it oml-uwsgi-server aws --endpoint http://{{ data_host }}:9000 s3 sync s3://{{ bucket_name }}/media_root/ {{ INSTALL_PREFIX }}media_root/"
      when:
        - inventory_hostname in groups['omnileads_app']
        - container_orchest == "systemd"
        - bucket_url is not defined
        - bucket_ssl_selfsigned is not defined

    - name: RESTORE django media_root from selfhosted BUCKET AIO
      shell: "podman exec -it oml-uwsgi-server aws --endpoint http://localhost:9000 s3 sync s3://{{ bucket_name }}/media_root {{ INSTALL_PREFIX }}media_root/"
      when:
        - inventory_hostname in groups['omnileads_aio']
        - container_orchest == "systemd"
        - bucket_url is not defined
        - bucket_ssl_selfsigned is not defined
    
    - name: RESTORE django media_root from external BUCKET 
      shell: "podman exec -it oml-uwsgi-server aws --endpoint {{ bucket_url }} s3 sync s3://{{ bucket_name }}/media_root {{ INSTALL_PREFIX }}media_root/"
      when:
        - inventory_hostname in groups['omnileads_app'] or inventory_hostname in groups['omnileads_aio']
        - container_orchest == "systemd"
        - bucket_url is defined
        - bucket_ssl_selfsigned is not defined

    - name: RESTORE django media_root from SSL selfsigned BUCKET
      shell: "podman exec -it oml-uwsgi-server aws --endpoint {{ bucket_url }} --no-verify-ssl s3 sync s3://{{ bucket_name }}/media_root {{ INSTALL_PREFIX }}media_root/"
      when:
        - inventory_hostname in groups['omnileads_app'] or inventory_hostname in groups['omnileads_aio']
        - container_orchest == "systemd"
        - bucket_url is defined
        - bucket_ssl_selfsigned is defined

    - name: RESTORE DJANGO Restart systemd service
      systemd:
        name: omnileads.service
        state: restarted
        enabled: yes
        daemon_reload: yes
      when:
        - inventory_hostname in groups['omnileads_app'] or inventory_hostname in groups['omnileads_aio']
        - container_orchest == "systemd"

    - name: RESTORE DJANGO nginx
      systemd:
        name: nginx.service
        state: restarted
        enabled: yes
        daemon_reload: yes
      when:
        - inventory_hostname in groups['omnileads_app'] or inventory_hostname in groups['omnileads_aio']
        - container_orchest == "systemd"
